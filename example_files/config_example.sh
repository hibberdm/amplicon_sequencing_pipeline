#!/bin/sh

# Purpose:   Provide a central location where variables utilized by the 16S (V4) pipeline can be defined
# Reasoning: Defining all variables in one place makes it easier to review/adjust settings
#            quickly and without needing to propagate changes across multiple steps/scripts
# Location:  This file should be stored within the top level of your project folder

# General (project-level)
#########################
WORK_DIR=$( pwd )
export WORK_DIR
export LOGS_DIR=${WORK_DIR}/logs
export DEMUX_DIR=${WORK_DIR}/demux
export P515F_DIR=${DEMUX_DIR}/515F
export P806R_DIR=${DEMUX_DIR}/806R
export PAIRED_DIR=${WORK_DIR}/paired       # This is not yet customizable; do not change
export FILTERED_DIR=${WORK_DIR}/filtered   # This is not yet customizable (or used); do not change
export MAPPING_FILE_PATH=${WORK_DIR}/<mapping file name>

# 1_remove_primer_seqs_BBTools
##############################
export PREPROCESSING_CPU='1'
export PREPROCESSING_MEM='512M'
export PREPROCESSING_NOTIFY='END,FAIL,REQUEUE'
export PREPROCESSING_EMAIL=''
export PREPROCESSING_LOGS_BASENAME='1_remove_primer_seqs_BBTools'

# 2_trim_and_filter_DADA2
#########################
export FILTER_CPU='1'
export FILTER_MEM='1G'
export FILTER_NOTIFY='END,FAIL,REQUEUE'
export FILTER_EMAIL=''
export FILTER_LOGS_BASENAME='2_trim_and_filter_DADA2'
export FILTER_TRUNCLEN_R1=200
export FILTER_TRUNCLEN_R2=150
export FILTER_MAXEE_R1=2
export FILTER_MAXEE_R2=2

# 3_call_ASVs_DADA2
###################
export DADA2_CPU='8'
export DADA2_MEM='24G'
export DADA2_NOTIFY='END,FAIL,REQUEUE'
export DADA2_EMAIL=''
export DADA2_LOGS_BASENAME='3_call_ASVs_DADA2'


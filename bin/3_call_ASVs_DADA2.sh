#!/bin/bash

################################################
# 3. Perform ASV calling using DADA2
################################################

# Description:
#   Using the DADA2 R package, merge and perform ASV calling on the read pairs that have 
#   survived preprocessing (primer matching/removal, quality-based filtering), and create an output
#   file containing the ASV table (encoded as an R object) 

# This script submits a non-array job using sbatch, passing its options via the
# command line (instead of within the .sbatch script itself).
# This allows me to:
#    -Use interpolation to pull SBATCH options from the config.sh file

# Retrieve definitions for variables not defined within this script
source "./config.sh"

STEP_LOGS_DIR=${LOGS_DIR}/${DADA2_LOGS_BASENAME}
if [ ! -d ${STEP_LOGS_DIR} ]; then mkdir -p ${STEP_LOGS_DIR}; fi

# Reminder: .sbatch file must be specified AFTER sbatch options for those options to be considered
sbatch \
        --cpus-per-task ${DADA2_CPU} \
	--mem ${DADA2_MEM} \
        --mail-type ${DADA2_NOTIFY} \
        --mail-user ${DADA2_EMAIL} \
        --output ${STEP_LOGS_DIR}/${DADA2_LOGS_BASENAME}_%A.txt \
        3_call_ASVs_DADA2.sbatch

#!/bin/sh

# Running this script retrieves a copy of folder 'example_files' which contains two types of resources:

# 1) Example templates for the two inputs that must be provided when running the workflow:
#	mapping_file_example.txt
#	config_example.sh

# 2) Example code that can be used as a starting point for thinking about next steps after
#   a preliminary ASV table has been generated.
#	analysis/
#	modeling/

# The example_files folder exists within the Spack folder system for the ASV workflow but is not
# included in the user's PATH by default when the package is loaded.

# Folder example_files can be found one directory level up from the /bin folder in which
# this script resides. Use the full path to this script to locate the example_files folder.

WORKFLOW_BIN_PATH=$(dirname $(which $0))
EXAMPLE_FILES_PATH=${WORKFLOW_BIN_PATH}/../example_files
echo -e 'Creating a copy of folder' $(realpath $EXAMPLE_FILES_PATH) '\b...'
cp -r ${EXAMPLE_FILES_PATH} .
